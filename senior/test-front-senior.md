
# QA technical test - Front


## **Aircall context…**

### You work at Aircall….

The feature team is releasing a new version of our application which is a phone that you can use within a browser (only google chrome). 
We created an account for you with a number associated that you can use to receive and make calls. 
All basic functionalities are available and working and we want to ensure that they are not broken.


### Goals…
1. Write a test plan and automate what you think is relevant
2. Be accurate. Explain your choices (test plan, tools etc…). <br/> 
Keep in mind that we are interested in technical skills as much as logic and mindset.

### Tools…
Free, use the tools you want

### Go beyond…
Add any relevant contribution you feel is important. As a QA Team, we own testing!

### Your feedback…Send us:
* Your test plan
* Your automated test
* A readme file explaining how to run it (a video works as well)

### Test details…
* A google doc is ok

* Phone1 URL: https://phone.aircall.io/ <br/>
Login: [...]<br/>
Passord: [...]

* Phone2 URL: https://phone.aircall.io/ <br/>
Login: [...]<br/>
Passord: [...]

##Infos
Be aware that when you're looking for a number in the contact page you're looking for a teammate number which will not be an external call.
In order to call the other phone and have all functionalities (assignation, comments etc..) you have to call the number by typing it or by creating a contact with this number
